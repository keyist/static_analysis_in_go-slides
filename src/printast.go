// from http://golang.org/pkg/go/ast/#example_Print
package main

import (
	"go/ast"
	"go/parser"
	"go/token"
)

func main() {
	src := ` package main; func main() { println("Hello, World!") } ` // HL
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "", src, 0)
	if err != nil {
		panic(err)
	}
	ast.Print(fset, f)
}
