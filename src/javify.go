package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"os"
	"strings"
)

type FuncDeclVisitor struct{}

func (v *FuncDeclVisitor) Visit(node ast.Node) ast.Visitor { // HLvisit
	switch t := node.(type) {
	case *ast.FuncDecl:
		t.Name = ast.NewIdent(fmt.Sprintf("Abstract%sFactory", strings.Title(t.Name.Name)))
		return nil // HLreturn
	}
	return v // HLreturn
}

func main() {
	fset := token.NewFileSet()
	file, err := parser.ParseFile(fset, os.Args[1], nil, 0)
	if err != nil {
		panic(err)
	}
	ast.Walk(new(FuncDeclVisitor), file) // HLwalk
	printer.Fprint(os.Stdout, fset, file)
}
