Static Analysis in Go

Tim Goh
Nitrous.IO
@keyist

* Static analysis

- Analyze software
- Without executing it
- Not limited to statically-typed languages!

# Introduce static analysis, call out how 'static' here is not used the same way as in static typing

* Static analysis for dummies

  for _, file := range files {
  	if lineCount(file) > 10000 {
  		fmt.Printf("%s is too big, split it up.", file)
  	}
  }

* What can you find in automated fashion?

.image images/automate-all-the-things.jpg

# Image credit: memegenerator.net

- repeated definitions
- repeated lines
- unreachable code
- complexity heuristics

* Some languages are harder to analyze

  class Zombie
    def undead_method
      # some code
    end
  end

  Zombie.new.send(%w(undead method).join('_'))

Can't just check for calls to the exact name 'undead_method'

* What's out there?

* Coverity

.image https://communities.coverity.com/servlet/JiveServlet/showImage/2-5098-1375/Screenshot_1_28_13_1_14_PM.png 400 800

# Image credit: https://communities.coverity.com/thread/2667

* CodeClimate

.image https://codeclimate.com/assets/type/tour/branch-compare.jpg

# Image credit: https://codeclimate.com/tour

* go tools

- go fmt
- go vet
- go fix

`go vet` can find unreachable code, amongst other things.

Audience participation: in Go, what do you look for to determine unreachable?

* Abstract Syntax Trees

.image https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Abstract_syntax_tree_for_Euclidean_algorithm.svg/797px-Abstract_syntax_tree_for_Euclidean_algorithm.svg.png 300 231

Corresponds to

  while b ≠ 0
      if a > b
          a := a − b
      else
          b := b − a
  return a

# Image credit: http://en.wikipedia.org/wiki/Abstract_syntax_trees

* language spec vs ast comparison

EBNF:

  IfStmt = "if" [ SimpleStmt ";" ] Expression Block [ "else" ( IfStmt | Block ) ] .

go/ast definition:

  type IfStmt struct {
          If   token.Pos // position of "if" keyword
          Init Stmt      // initialization statement; or nil
          Cond Expr      // condition
          Body *BlockStmt
          Else Stmt // else branch; or nil
  }

* AST printing

.code src/printast.go

Audience participation part 2: how many lines will be printed?

# i.e. how large is the AST for Hello World

* AST printing

.code output/printast-1.out

* AST printing

.code output/printast-2.out

* AST printing

.code output/printast-3.out

* Just reading is boring. Let's write

.image https://d3gqasl9vmjfd8.cloudfront.net/a8433b05-56d0-4eca-8c8d-627b037a7995.png

# Image credit: http://shirt.woot.com/offers/reading-rambo-vip-1

* Javify

.code src/javify.go /^type FuncDeclVisitor/,/^}/ HLvisit

.code src/javify.go /^func main/,/^}/ HLwalk

# Change Go code to make it more Java-like

* Javify

.code src/javify.go /^type FuncDeclVisitor/,/^}/ HLreturn

.code src/javify.go /^func main/,/^}/

* Javify on Javify

.code output/javify.out

* Javify**3

.code output/javify2.out

* Real-World applications

- transform code before tests
- instrument code for profiling
- preprocessor macros

* go vet deadcode

.image images/deadjim.jpg

* go vet deadcode

.link https://code.google.com/p/go/source/browse/cmd/vet/deadcode.go?repo=tools

  // findDead walks the statement looking for dead code.
  // If d.reachable is false on entry, stmt itself is dead.
  // When findDead returns, d.reachable tells whether the
  // statement following stmt is reachable.
  func (d *deadState) findDead(stmt ast.Stmt) {

  	// ...elided...
  	case *ast.ExprStmt:
  		// Call to panic?
  		call, ok := x.X.(*ast.CallExpr)
  		if ok {
  			name, ok := call.Fun.(*ast.Ident)
  			if ok && name.Name == "panic" && name.Obj == nil {
  				d.reachable = false
  			}
  		}

* go-faster

.link https://github.com/bouk/go-faster

Transforms

  package main

  func a(c int) int {
  	c += 1
  	return c
  }

  func main() {
  	println(a(2))
  }

* go-faster

Into

  package main

  func a(c int) chan int {
  	result := make(chan int)
  	go func() {
  		result <- func() int {
  			c += 1
  			return c
  		}()
  	}()
  	return result
  }

  func main() {
  	println(<-a(2))
  }

* Go/ast Rider

My semantic diff tool with a really dumb name

.link http://bitbucket.org/keyist/goastrider

.image images/goastrider-output.png

* Use cases

- has the interface of a dependency changed?
- have I broken my public API?

* Fun Take-Home Exercises

- find uses of keywords as struct field names
- find identifiers more than 20 characters long
- insert logging of "Now running FuncName" before each function
- find functions FuncName without a corresponding TestFuncName function

