Static Analysis in Go
=====================

BACKGROUND
----------

These are the slides for my talk at the Singapore Gophers meet-up on 2014-05-21 at the Nitrous.IO_ office.

.. _Nitrous.IO: https://www.nitrous.io

The goal of the talk was to give an overview of static analysis and actual
examples of how the go/ast package can be used.

Those who attended the talk will note that these slides are slightly modified.
When giving talks I generally leave some bullet points I'll be speaking about
off the slides (a) to avoid crowding (b) so I don't look like I'm just reading
from the screen.  The downside is that it makes the stand-alone slides look
incomplete.  I have made some minor additions.

TO VIEW
-------

This presentation is in go-present format.  You can view it at SpeakerDeck_, or clone this repo and run ``present`` in the project directory.

.. _SpeakerDeck: https://speakerdeck.com/keyist/static-analysis-in-go

sourcecode::

    go get http://code.google.com/p/go.tools/cmd/present
    go install http://code.google.com/p/go.tools/cmd/present
    git clone https://bitbucket.org/keyist/static_analysis_in_go-slides saig-slides
    cd saig-slides
    present

